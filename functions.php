<?php
/**
 * Version
 */

use IWP\ChangeHeader;
use IWP\WoocommercePresets;

const VERSION_THEME = '1.5.0';

/**
 * Child Load Script.
 */
function load_child_styles() {
	$thb_dependency = [ 'jquery', 'underscore' ];
	wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/style.css', '', VERSION_THEME );
	wp_deregister_script( 'thb-app' );
	wp_enqueue_script( 'thb-app', esc_url( get_theme_file_uri( 'assets/js/app.min.js' ) ), $thb_dependency, esc_attr( '' ), true );
	wp_localize_script(
		'thb-app',
		'themeajax',
		[
			'url'      => admin_url( 'admin-ajax.php' ),
			'l10n'     => [
				'loadmore'        => esc_html__( 'Load More', 'north' ),
				'loading'         => esc_html__( 'Loading ...', 'north' ),
				'nomore'          => esc_html__( 'All Posts Loaded', 'north' ),
				'nomore_products' => esc_html__( 'All Products Loaded', 'north' ),
				'results_found'   => esc_html__( 'results found.', 'north' ),
				'results_all'     => esc_html__( 'View All Results', 'north' ),
				'adding_to_cart'  => esc_html__( 'Adding to Cart', 'north' ),
				'added_to_cart'   => esc_html__( 'Added to Cart', 'north' ),
			],
			'nonce'    => [
				'product_quickview' => wp_create_nonce( 'thb_quickview_ajax' ),
				'autocomplete_ajax' => wp_create_nonce( 'thb_autocomplete_ajax' ),
			],
			'settings' => [
				'site_url'                        => get_home_url(),
				'shop_product_listing_pagination' => ot_get_option( 'shop_product_listing_pagination', 'style1' ),
				'posts_per_page'                  => get_option( 'posts_per_page' ),
				'newsletter'                      => ot_get_option( 'newsletter', 'on' ),
				'newsletter_length'               => ot_get_option( 'newsletter-interval', '1' ),
				'newsletter_delay'                => ot_get_option( 'newsletter_delay', '0' ),
				'is_cart'                         => thb_wc_supported() ? is_cart() : false,
				'is_checkout'                     => thb_wc_supported() ? is_checkout() : false,
				'header_quick_cart'               => ot_get_option( 'header_quick_cart', 'on' ),
			],
			'icons'    => [
				'close' => thb_load_template_part( 'assets/img/svg/close.svg' ),
			],
		]
	);
}

add_action( 'wp_enqueue_scripts', 'load_child_styles', 20 );

remove_action( 'thb_product_badge', 'thb_product_badge', 3 );

/**
 * Add Scripts
 */
function add_scripts() {
	wp_enqueue_script(
		'custom-script',
		get_stylesheet_directory_uri() . '/custom_script.js',
		[ 'jquery' ],
		VERSION_THEME,
		true
	);
}

add_action( 'wp_enqueue_scripts', 'add_scripts' );

/**
 * Save the option field as custom order meta, when checkbox has been checked.
 *
 * @param $order
 * @param $data
 */
function action_checkout_update_order_meta( $order, $data ) {
	if ( isset( $_POST['marketing_opting'] ) ) {
		$order->update_meta_data( '_marketing_opting', empty( $_POST['marketing_opting'] ) ? 'no' : 'yes' );
	}
}

add_action( 'woocommerce_checkout_create_order', 'action_checkout_update_order_meta', 10, 2 );

/**
 * Save the optin field as custom user meta, when checkbox has been checked.
 */

function action_checkout_update_customer_meta( $customer, $data ) {
	if ( isset( $_POST['marketing_opting'] ) ) {
		$customer->update_meta_data( 'marketing_opting', empty( $_POST['marketing_opting'] ) ? 'no' : 'yes' );
	}
}

add_action( 'woocommerce_checkout_update_customer', 'action_checkout_update_customer_meta', 10, 2 );

/**
 * Display the result of the checked optin in the order under billing address.
 *
 * @param $order
 */

function display_custom_field_on_order_edit_pages( $order ) {
	if ( $order->get_meta( '_marketing_opting' ) === 'yes' ) {
		echo '<p><strong>Has opted in for marketing purposes.</p>';
	}
}

add_action( 'woocommerce_admin_order_data_after_billing_address', 'display_custom_field_on_order_edit_pages', 10, 1 );

// 2. SHOW CUSTOM COLUMN FOR THE OPTIN OPTION.


/**
 * Adding custom column title.
 *
 * @param $columns
 *
 * @return mixed
 */
function custom_shop_order_column( $columns ) {
	$action_column = $columns['order_actions'];
	unset( $columns['order_actions'] );

	//add the new column "Opt in"
	$columns['order_marketing'] = '<div align="center">' . __( "Opted in?" ) . '</div>'; // title
	$columns['order_actions']   = $action_column;

	return $columns;
}

add_filter( 'manage_edit-shop_order_columns', 'custom_shop_order_column', 12, 1 );

/**
 * Add the data for each order.
 *
 * @param $column
 * @param $post_id
 */
function custom_order_list_column_content( $column, $post_id ) {
	global $post, $the_order;

	if ( $column === 'order_marketing' ) {
		$value = $the_order->get_meta( '_marketing_opting' );
		$label = $value === 'yes' ? __( 'Signed Up' ) : ucfirst( $value );
		$color = $value === 'yes' ? 'color:#00cc00;' : 'color:#bbbbbb;';

		echo '<p align="center" style="' . esc_attr( $color ) . '"><span class="dashicons dashicons-' . esc_attr( $value ) . '"></span><span style="font-weight:600;">' . esc_html( $label ) . '</span></p>';
	}
}

add_action( 'manage_shop_order_posts_custom_column', 'custom_order_list_column_content', 10, 2 );

// 3. Make marketing optin meta searchable from search field (can't work very well for 'yes' or 'no' values!)


/**
 * Make a custom meta field searchable from the admin order list search field.
 *
 * @param $meta_keys
 *
 * @return mixed
 */
function marketing_search_fields( $meta_keys ) {
	$meta_keys[] = '_marketing_opting';

	return $meta_keys;
}

add_filter( 'woocommerce_shop_order_search_fields', 'marketing_search_fields', 10, 1 );
// 4. Add a dropdown filter to get orders by marketing optin meta value

/**
 * Add a dropdown to filter orders by Marketing optin.
 */
function display_admin_shop_order_marketing_opting_filter() {
	global $pagenow, $post_type;

	if ( 'shop_order' === $post_type && 'edit.php' === $pagenow ) {
		$domain  = 'woocommerce';
		$current = isset( $_GET['filter_shop_order_marketing'] ) ? esc_attr( wp_unslash( $_GET['filter_shop_order_marketing'] ) ) : '';

		echo '<select name="filter_shop_order_marketing">
        <option value="">' . __( 'Filter Marketing optin', $domain ) . '</option>';

		$options = [ 'yes' => __( 'Signed Up' ), 'no' => __( 'No' ) ];

		foreach ( $options as $key => $label ) {
			printf( '<option value="%s"%s>%s</option>', $key,
				$key === $current ? '" selected="selected"' : '', $label );
		}
		echo '</select>';
	}
}

add_action( 'restrict_manage_posts', 'display_admin_shop_order_marketing_opting_filter' );


/**
 * Process the filter dropdown for orders by Marketing optin.
 *
 * @param $vars
 *
 * @return mixed
 */
function process_admin_shop_order_marketing_opting_filter( $vars ) {
	global $pagenow, $typenow;

	if ( $pagenow == 'edit.php' && isset( $_GET['filter_shop_order_marketing'] )
	     && $_GET['filter_shop_order_marketing'] != '' && 'shop_order' === $typenow ) {
		$vars['meta_key']   = '_marketing_opting';
		$vars['meta_value'] = wc_clean( $_GET['filter_shop_order_marketing'] );
	}

	return $vars;
}

add_filter( 'request', 'process_admin_shop_order_marketing_opting_filter', 99 );

/**
 * Start Alex L
 */
require_once __DIR__ . '/vendor/autoload.php';

new ChangeHeader();
new WoocommercePresets();
/**
 * Theme after_setup_theme.
 *
 * @return void
 */
function setup_theme(): void {
	\Carbon_Fields\Carbon_Fields::boot();
	remove_action( 'woocommerce_single_product_summary', 'thb_sizing_guide', 37 );
}

add_action( 'after_setup_theme', 'setup_theme' );

/**
 * Add Custom HTML block to Customize.
 */
function add_customize() {
	new \IWP\CustomizeTheme();
}

add_action( 'customize_register', 'add_customize' );

/**
 * Add support svg upload.
 *
 * @param array $mimes Mimes Type.
 *
 * @return mixed
 */
function add_svg_upload( array $mimes ): array {
	$mimes['svg']  = 'image/svg+xml';
	$mimes['svgz'] = 'image/svg+xml';

	return $mimes;
}

add_filter( 'upload_mimes', 'add_svg_upload' );
