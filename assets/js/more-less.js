jQuery( document ).ready( function( $ ) {
	$( '.time' ).hide();
	const decription = $( '.woocommerce-product-details__short-description' );
	$( decription.children( 'p' )[ 0 ] ).wrapAll( '<div class=\'short\' />' );
	let el = decription.children( '*' );
	let newArray = [];
	$.each( el, function( index, value ) {
		if ( 0 != index ) {
			newArray.push( value )
		}
	} );
	$( newArray ).wrapAll( '<div class=\'more\' />' );
	decription.append( `<a href="#" class="more-btn btn alt small">${iwp_more_less.text_more}</a>` );
	const moreBtn = $( '.more-btn' );
	let flag = false;
	$( moreBtn ).click( function( e ) {
		e.preventDefault();
		if ( ! flag ) {
			$( '.more' ).show();
			flag = ! flag;
			$( moreBtn ).text( iwp_more_less.text_less )
		} else {
			$( '.more' ).hide();
			flag = ! flag;
			$( moreBtn ).text( iwp_more_less.text_more )
		}
	} );
	let colorFlag = false;
	let sizeFlag = false;
	$( '#pa_color' ).change( function( e ) {
		colorFlag = true;
		hideNotice();
	} );
	$( '#pa_size' ).change( function( e ) {
		sizeFlag = true;
		hideNotice();
	} );

	function hideNotice() {
		if ( sizeFlag && colorFlag ) {
			$( '.woocommerce-variation-add-to-cart p.description' ).css( 'opacity', '0' )
		}
	}

	$( '.reset_variations' ).click( function( e ) {
		setTimeout( function() {
			sizeFlag = false;
			colorFlag = false;
			$( '.woocommerce-variation-add-to-cart p.description' ).css( 'opacity', '1' )
		}, 200 )
	} );

	$( '#next_slide' ).click( function( e ) {
		e.preventDefault();
		jQuery( '.carousel' ).slick( 'slickNext' )
	} );

	$( '#prev_slide' ).click( function( e ) {
		e.preventDefault();
		jQuery( '.carousel' ).slick( 'slickPrev' )
	} );

	setTimeout( function() {
		jQuery( '.date_available a' ).click( function( e ) {
			console.log('click')
			jQuery( '.time' ).show();
		} );
	}, 500 );

	setTimeout( function() {
		$( '.woocommerce-variation-add-to-cart .single_add_to_cart_button' ).removeClass( 'disabled' );
	}, 2000 );

	$( '.woocommerce-variation-add-to-cart .single_add_to_cart_button' ).click( function( e ) {

		if ( ! sizeFlag ) {
			console.log( 'size' )
			$( '.thb_box-swatch span' ).css( 'border-color', 'red' );
		}

		if ( ! colorFlag ) {
			console.log( 'color' )
			$( '.swatch-color' ).css( 'border', '1px solid red' );
		}

	} );

	$( '.thb_box-swatch span' ).click( function( e ) {
		$( '.thb_box-swatch span' ).css( 'border-color', '' );
	} );

	$( '.swatch-color' ).click( function( e ) {
		$( '.swatch-color' ).css( 'border', '' );
	} );

} );