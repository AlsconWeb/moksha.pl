<?php
/**
 * Single Product Price
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/price.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

$current_lang = apply_filters( 'wpml_current_language', null );

?>
<p class="<?php echo esc_attr( apply_filters( 'woocommerce_product_price_class', 'price' ) ); ?>"><?php echo $product->get_price_html(); ?></p>
<!-- Placement v2 -->
<klarna-placement
		data-key="credit-promotion-badge"
		data-locale="<?php echo esc_html( ICL_LANGUAGE_CODE . '-' . strtoupper( ICL_LANGUAGE_CODE ) ); ?>"
		data-purchase-amount="<?php echo $product->get_price() * 100; ?>"
></klarna-placement>
<!-- end Placement -->
