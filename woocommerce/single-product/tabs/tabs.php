<?php
/**
 * Single Product tabs
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/tabs/tabs.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.8.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Filter tabs and allow third parties to add their own.
 *
 * Each tab is an array containing title, callback and priority.
 * @see woocommerce_default_product_tabs()
 */
$product_tabs            = apply_filters( 'woocommerce_product_tabs', [] );
$shop_product_tabs_style = ot_get_option( 'shop_product_tabs_style', 'style1' );
$column_classes          = 'style1' === $shop_product_tabs_style ? 'medium-10 large-7' : 'large-8';
if ( ! empty( $product_tabs ) ) : ?>
	<div class="row align-center">
		<div class="small-12 <?php echo esc_attr( $column_classes ); ?> columns">
			<div class="woocommerce-tabs wc-tabs-wrapper <?php echo esc_attr( $shop_product_tabs_style ); ?>">
				<ul class="tabs wc-tabs" role="tablist">
					<?php foreach ( $product_tabs as $key => $tab ) : ?>
						<li class="<?php echo esc_attr( $key ); ?>_tab" id="tab-title-<?php echo esc_attr( $key ); ?>"
							role="tab" aria-controls="tab-<?php echo esc_attr( $key ); ?>">
							<a href="#tab-<?php echo esc_attr( $key ); ?>"><?php echo apply_filters( 'woocommerce_product_' . $key . '_tab_title', esc_html( $tab['title'] ), $key ); ?></a>
						</li>
					<?php endforeach; ?>
				</ul>
				<div class="woocommerce-Tabs-panel-wrapper">
					<?php foreach ( $product_tabs as $key => $tab ) : ?>
						<div class="woocommerce-Tabs-panel woocommerce-Tabs-panel--<?php echo esc_attr( $key ); ?> panel entry-content wc-tab"
							 id="tab-<?php echo esc_attr( $key ); ?>" role="tabpanel"
							 aria-labelledby="tab-title-<?php echo esc_attr( $key ); ?>">
							<?php
							if ( isset( $tab['callback'] ) ) {
								call_user_func( $tab['callback'], $key, $tab );
							}
							?>
						</div>
					<?php endforeach; ?>
					<?php do_action( 'woocommerce_product_after_tabs' ); ?>
				</div>
			</div>
			<?php
			$tax_product    = get_post_meta( get_the_ID(), 'tax_product', true );
			$costs_product  = get_post_meta( get_the_ID(), 'costs_product', true );
			$margin_product = get_post_meta( get_the_ID(), 'margin_product', true );
			$error_percent  = get_post_meta( get_the_ID(), 'error_percent', true );

			if ( ! $error_percent && ! empty( $costs_product ) ) :
				?>
				<div class="product-transparency">
					<section class="transparency-of-price">
						<h3 class="transparency-of-price-header"><?php esc_html_e( 'Price transparency', 'north' ); ?></h3>
						<div class="column-text">
							<?php
							global $product;

							echo sprintf(
								'<p>%s: %s %s:</p>',
								esc_html__( 'Total price', 'north' ),
								wp_kses_post( wc_price( $product->get_price() ) ),
								esc_html__( 'which it consists of', 'north' )
							);
							?>
						</div>
						<div class="d-flex align-items-center">
							<div class="transparency-bar">
								<div
										class="transparency-item transparency-item--first"
										style="width: <?php echo esc_attr( $costs_product ); ?>%;">
									<span class="transparency-item-span"><?php echo esc_attr( $costs_product ); ?>%</span>
								</div>
								<div class="transparency-item" style="width: <?php echo esc_attr( $tax_product ); ?>%;">
									<span class="transparency-item-span"><?php echo esc_attr( $tax_product ); ?>%</span>
								</div>
								<div
										class="transparency-item transparency-item--last"
										style="width: <?php echo esc_attr( $margin_product ); ?>%;">
									<span class="transparency-item-span"><?php echo esc_attr( $margin_product ); ?>%</span>
								</div>
							</div>
						</div>
						<div class="fituresRow">
							<div class="fituresRow-inner1">
								<div class="fituresRow-inner1-fav1"></div>
								<div class="fituresRow-inner1-text">
									<h5><?php esc_html_e( 'COSTS OF PRODUCTION', 'north' ); ?></h5>
								</div>
							</div>
							<div class="fituresRow-inner2">
								<div class="fituresRow-inner1-fav2"></div>
								<div class="fituresRow-inner1-text">
									<h5><?php esc_html_e( 'TAX', 'north' ); ?></h5>
								</div>
							</div>
							<div class="fituresRow-inner3">
								<div class="fituresRow-inner1-fav3"></div>
								<div class="fituresRow-inner1-text">
									<h5><?php esc_html_e( 'FIXED COSTS AND MARGIN', 'north' ); ?></h5>
								</div>
							</div>
						</div>
					</section>
				</div>
			<?php endif; ?>
		</div>
	</div>
<?php
endif;
