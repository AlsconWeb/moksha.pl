<?php
/**
 * Change Header.
 *
 * @package IWP
 */

namespace IWP;

/**
 * ChangeHeader class file.
 */
class ChangeHeader {
	/**
	 * ChangeHeader construct.
	 */
	public function __construct() {
		add_action( 'init', [ $this, 'init' ] );
	}

	/**
	 * Init class ChangeHeader.
	 */
	public function init(): void {
		remove_action( 'thb_secondary_area', 'thb_secondary_area' );
		add_action( 'thb_secondary_area', [ $this, 'show_second_section' ] );
	}

	/**
	 * Change and show second section.
	 */
	public function show_second_section(): void {
		$header_secondary_style = ot_get_option( 'header_secondary_style', 'style1' );

		if ( 'style1' === $header_secondary_style ) {
			if ( has_nav_menu( 'acc-menu-in' ) && is_user_logged_in() ) {
				wp_nav_menu(
					[
						'theme_location' => 'acc-menu-in',
						'depth'          => 2,
						'container'      => false,
						'menu_class'     => 'secondary-menu',
					]
				);
			} elseif ( has_nav_menu( 'acc-menu-out' ) && ! is_user_logged_in() ) {
				wp_nav_menu(
					[
						'theme_location' => 'acc-menu-out',
						'depth'          => 2,
						'container'      => false,
						'menu_class'     => 'secondary-menu',
					]
				);
			}
			if ( 'on' === ot_get_option( 'thb_ls', 'on' ) ) {
				do_action( 'thb_language_switcher' );
			}
		} else {
			if ( 'on' === ot_get_option( 'thb_ls', 'on' ) ) {
				do_action( 'thb_language_switcher' );
			}
			if ( 'on' === ot_get_option( 'header_myaccount', 'on' ) ) {
				do_action( 'thb_quick_profile' );
			}
		}
		do_action( 'thb_footer_social' );
		do_action( 'thb_quick_search', $header_secondary_style );
		do_action( 'thb_quick_cart', $header_secondary_style );
	}
}
