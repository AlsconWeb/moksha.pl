<?php
/**
 * Created 04.08.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 *
 * @package IWP\Customize
 */

namespace IWP;

/**
 * Class CustomizeTheme.
 */
class CustomizeTheme {
	/**
	 * Connect to WP_Customize_Manager class.
	 *
	 * @var \WP_Customize_Manager
	 */
	public $customize;

	/**
	 * Constructor CustomizeTheme class.
	 */
	public function __construct() {
		global $wp_customize;

		$this->customize = $wp_customize;

		$this->add_section();

		$this->general_settings();
	}

	/**
	 * Add Section to Customize.
	 */
	public function add_section(): void {
		$this->customize->add_section(
			'general',
			[
				'title'    => __( 'Custom Settings', 'north' ),
				'priority' => 70,
			]
		);

	}

	/**
	 * Add General Settings Field.
	 */
	public function general_settings(): void {

		$this->customize->add_setting(
			'iwp_custom_html',
			[
				'transport' => 'refresh',
				'height'    => 325,
			]
		);
		$this->customize->add_control(
			'iwp_custom_html',
			[
				'section' => 'general',
				'label'   => __( 'Custom HTML', 'north' ),
				'type'    => 'textarea',
			]
		);

		$this->customize->add_setting(
			'iwp_contact_style',
			[
				'transport' => 'refresh',
				'height'    => 325,
			]
		);
		$this->customize->add_control(
			'iwp_contact_style',
			[
				'section' => 'general',
				'label'   => __( 'Contact with our Stylist', 'north' ),
				'type'    => 'textarea',
			]
		);

		$this->customize->add_setting(
			'iwp_coming_soon',
			[
				'transport' => 'refresh',
				'height'    => 325,
			]
		);
		$this->customize->add_control(
			'iwp_coming_soon',
			[
				'section' => 'general',
				'label'   => __( 'Coming soon shortcode form', 'north' ),
				'type'    => 'textarea',
			]
		);
	}
}
