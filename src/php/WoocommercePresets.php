<?php
/**
 * Add more/less do description product.
 *
 * @package IWP
 */

namespace IWP;

use Carbon_Fields\Container;
use Carbon_Fields\Field;

/**
 * HideProductDescription file.
 */
class WoocommercePresets {

	/**
	 * Array terms fields.
	 *
	 * @var array $fieds_checkbox Fields args.
	 */
	private $fieds_checkbox;

	/**
	 * HideProductDescription construct.
	 */
	public function __construct() {
		$this->init();
		new ChangeProductCard();
		remove_action( 'thb_product_badge', 'thb_product_badge', 3 );

	}

	/**
	 * Init.
	 */
	public function init(): void {

		$klarna_settings                    = get_option( 'woocommerce_klarna_payments_settings' );
		$this->klarna_setting_hook_priority = $klarna_settings['onsite_messaging_product_location'];

		$this->fieds_checkbox = [
			[
				'name' => 'terms_and_conditions',
				'arg'  => [
					'type'     => 'checkbox',
					'id'       => 'terms_and_conditions',
					'required' => true,
					'class'    => [ 'input-checkbox' ],
					'label'    => __( 'I have read and accept the terms and conditions and privacy policy of the online store www.moksha.pl.', 'north' ),
				],
			],
			[
				'name' => 'personal_data',
				'arg'  => [
					'type'     => 'checkbox',
					'class'    => [ 'input-checkbox' ],
					'required' => true,
					'label'    => __( 'I agree to the processing of my personal data by Moksha SP.z o. o. for the purpose of order fulfillment.', 'north' ),
				],
			],
			[
				'name' => 'marketing_opting',
				'arg'  => [
					'type'  => 'checkbox',
					'class' => [ 'input-checkbox' ],
					'label' => __( 'I agree to receive a newsletter with information about promotions and news.', 'north' ),
				],
			],
		];

		add_action( 'wp_enqueue_scripts', [ $this, 'add_script' ] );
		add_filter( 'woocommerce_catalog_orderby', [ $this, 'filter_catalog_order_by' ] );
		add_action( 'woocommerce_after_checkout_validation', [ $this, 'validate_checkbox' ], 100, 2 );
		add_action( 'woocommerce_checkout_before_terms_and_conditions', [ $this, 'add_terms_checkbox' ] );
		add_action( 'woocommerce_checkout_update_order_meta', [ $this, 'save_data' ] );
		add_action( 'woocommerce_admin_order_data_after_billing_address', [ $this, 'output_in_order' ] );
		add_filter( 'woocommerce_product_data_tabs', [ $this, 'add_custom_tabs_in_metabox' ] );
		add_action( 'woocommerce_product_data_panels', [ $this, 'add_fields_custom_tab' ] );
		add_action( 'woocommerce_product_data_panels', [ $this, 'add_fields_price_percentage_tab' ] );
		add_action( 'woocommerce_process_product_meta', [ $this, 'save_custom_html' ] );
		add_action( 'woocommerce_process_product_meta', [ $this, 'save_fields_price_percentage' ] );
		add_action( 'woocommerce_single_product_summary', [ $this, 'add_custom_html' ], 20 );
		add_action( 'woocommerce_before_single_variation', [ $this, 'add_sizing_guide' ], 25 );
		add_action( 'woocommerce_widget_shopping_cart_buttons', [ $this, 'change_checkout_button' ], 20 );
		add_action( 'carbon_fields_register_fields', [ $this, 'add_carbon_field' ] );
		add_action( 'after_setup_theme', [ $this, 'add_image_size' ] );
		add_action( 'thb_product_badge', [ $this, 'new_product_badge' ], 3 );


		add_filter( 'woocommerce_get_stock_html', [ $this, 'custom_variable_html' ], 50, 2 );

		remove_action( 'woocommerce_widget_shopping_cart_buttons', 'woocommerce_widget_shopping_cart_proceed_to_checkout', 20 );

		if ( class_exists( 'WC_Pre_Orders_Product' ) ) {
			add_action(
				'woocommerce_before_single_variation',
				[
					new \WC_Pre_Orders_Product(),
					'add_pre_order_product_message',
				],
				35
			);

			remove_action(
				'woocommerce_single_product_summary',
				[
					new \WC_Pre_Orders_Product(),
					'add_pre_order_product_message',
				],
				11
			);
		}
	}

	/**
	 * Add Script.
	 */
	public function add_script(): void {
		wp_enqueue_script(
			'more-less-function',
			get_stylesheet_directory_uri() . '/assets/js/more-less.min.js',
			[ 'jquery' ],
			VERSION_THEME,
			true
		);

		wp_localize_script(
			'more-less-function',
			'iwp_more_less',
			[
				'text_more' => __( 'More', 'north' ),
				'text_less' => __( 'Less', 'north' ),
			]
		);
	}

	/**
	 * Unset rating order.
	 *
	 * @param array $array Array order by.
	 *
	 * @return array
	 */
	public function filter_catalog_order_by( array $array ): array {
		unset( $array['rating'] );

		return $array;
	}

	/**
	 * Add terms checkbox.
	 */
	public function add_terms_checkbox(): void {
		global $woocommerce;

		foreach ( $this->fieds_checkbox as $field ) {
			echo "<div id='" . esc_attr( $field['name'] ) . "'>";
			woocommerce_form_field(
				$field['name'],
				$field['arg'],
				$woocommerce->checkout->get_value( $field['name'] )
			);
			echo '</div>';
		}

	}

	/**
	 * Add validation.
	 *
	 * @param array $fields Fields.
	 * @param mixed $errors Errors.
	 */
	public function validate_checkbox( array $fields, $errors ): void {
		$terms_and_conditions = isset( $_POST['terms_and_conditions'] ) ? filter_var( wp_unslash( $_POST['terms_and_conditions'] ), FILTER_SANITIZE_NUMBER_INT ) : false;
		$personal_data        = isset( $_POST['personal_data'] ) ? filter_var( wp_unslash( $_POST['personal_data'] ), FILTER_SANITIZE_NUMBER_INT ) : false;

		if ( ! $terms_and_conditions ) {
			$errors->add( 'validation', __( 'Please indicate that you have read and agree to the Processing of the Personal Data.', 'north' ) );
		}

		if ( ! $personal_data ) {
			$errors->add( 'validation', __( 'Please indicate that you have read and agree to the Terms and Conditions and Provacy policy.', 'north' ) );
		}
	}

	/**
	 * Save Custom data.
	 *
	 * @param int $order_id Order Id.
	 */
	public function save_data( int $order_id ): void {
		$terms_and_conditions = isset( $_POST['terms_and_conditions'] ) ? filter_var( wp_unslash( $_POST['terms_and_conditions'] ), FILTER_SANITIZE_NUMBER_INT ) : false;
		$personal_data        = isset( $_POST['personal_data'] ) ? filter_var( wp_unslash( $_POST['personal_data'] ), FILTER_SANITIZE_NUMBER_INT ) : false;
		$marketing_opting     = isset( $_POST['marketing_opting'] ) ? filter_var( wp_unslash( $_POST['marketing_opting'] ), FILTER_SANITIZE_NUMBER_INT ) : false;

		if ( $terms_and_conditions ) {
			update_post_meta( $order_id, 'iwp_terms', $terms_and_conditions );
		}

		if ( $personal_data ) {
			update_post_meta( $order_id, 'iwp_personal_data', $personal_data );
		}

		if ( $marketing_opting ) {
			update_post_meta( $order_id, 'iwp_marketing_opting', $marketing_opting );
		}
	}

	/**
	 * Output custom fields in order.
	 *
	 * @param object $order Order.
	 */
	public function output_in_order( $order ): void {
		$terms            = get_post_meta( $order->id, 'iwp_terms', true ) ? __( 'Yes', 'north' ) : __( 'No', 'north' );
		$personal         = get_post_meta( $order->id, 'iwp_terms', true ) ? __( 'Yes', 'north' ) : __( 'No', 'north' );
		$marketing_opting = get_post_meta( $order->id, 'iwp_marketing_opting', true ) ? __( 'Yes', 'north' ) : __( 'No', 'north' );

		echo '<p><strong>' . esc_html( __( 'Terms and Conditions', 'north' ) ) . ':</strong> ' . esc_html( $terms ) . '</p>';
		echo '<p><strong>' . esc_html( __( 'Personal Data', 'north' ) ) . ':</strong> ' . esc_html( $personal ) . '</p>';
		echo '<p><strong>' . esc_html( __( 'Marketing Opting', 'north' ) ) . ':</strong> ' . esc_html( $marketing_opting ) . '</p>';
	}

	/**
	 * Add Custom tabs in  Meta box woocommerce.
	 *
	 * @param array $tabs Tabs args.
	 */
	public function add_custom_tabs_in_metabox( array $tabs ): array {
		$tabs['iwp_tab'] = [
			'label'    => __( 'Custom Settings', 'north' ),
			'target'   => 'iwp_custom_tab',
			'class'    => [],
			'priority' => 21,
		];

		$tabs['iwp_price_percentage_tab'] = [
			'label'    => __( 'Price transparency', 'north' ),
			'target'   => 'iwp_price_percentage_tab',
			'class'    => [],
			'priority' => 22,
		];

		return $tabs;
	}

	/**
	 * Add Field in custom tab.
	 */
	public function add_fields_custom_tab(): void {

		echo '<div id="iwp_custom_tab" class="panel woocommerce_options_panel hidden">';

		woocommerce_wp_textarea_input(
			[
				'id'          => 'iwp_custom_html',
				'value'       => get_post_meta( get_the_ID(), 'iwp_custom_html', true ),
				'label'       => __( 'Custom HTML', 'north' ),
				'desc_tip'    => true,
				'description' => __( 'Add Custom Html to product description. It will be displayed in the topic after the tags', 'north' ),
				'style'       => 'height: 400px;',
			]
		);
		woocommerce_wp_textarea_input(
			[
				'id'          => 'iwp_contact_style',
				'value'       => get_post_meta( get_the_ID(), 'iwp_contact_style', true ),
				'label'       => __( 'Contact with our Stylist', 'north' ),
				'desc_tip'    => true,
				'description' => __( 'Add Custom Html to product description. It will be displayed in the topic after the tags', 'north' ),
				'style'       => 'height: 400px;',
			]
		);

		woocommerce_wp_checkbox(
			[
				'id'          => 'is_coming_soon',
				'value'       => get_post_meta( get_the_ID(), 'is_coming_soon', true ),
				'label'       => __( 'Label in Photo coming soon', 'north' ),
				'description' => __( 'Check the checkbox if you want the inscription coming soon to appear on the photo', 'north' ),
				'desc_tip'    => true,
			]
		);

		echo '</div>';
	}

	/**
	 * Add fields price percentage tab.
	 */
	public function add_fields_price_percentage_tab(): void {

		$error = (int) get_post_meta( get_the_ID(), 'error_percent', true );

		echo '<div id="iwp_price_percentage_tab" class="panel woocommerce_options_panel hidden">';

		if ( $error ) {
			$this->notification();
		}

		woocommerce_wp_text_input(
			[
				'id'                => 'iwp_costs_product',
				'value'             => get_post_meta( get_the_ID(), 'costs_product', true ),
				'label'             => __( 'Costs of product', 'north' ),
				'name'              => 'costs_product',
				'type'              => 'number',
				'custom_attributes' => [
					'min' => 0,
					'max' => 100,
				],
			]
		);

		woocommerce_wp_text_input(
			[
				'id'                => 'iwp_tax',
				'value'             => get_post_meta( get_the_ID(), 'tax_product', true ),
				'label'             => __( 'Tax', 'north' ),
				'name'              => 'tax_product',
				'type'              => 'number',
				'custom_attributes' => [
					'min' => 0,
					'max' => 100,
				],
			]
		);

		woocommerce_wp_text_input(
			[
				'id'                => 'iwp_margin',
				'value'             => get_post_meta( get_the_ID(), 'margin_product', true ),
				'label'             => __( 'Fixed cost and margin', 'north' ),
				'name'              => 'margin_product',
				'type'              => 'number',
				'custom_attributes' => [
					'min' => 0,
					'max' => 100,
				],
			]
		);
		echo '</div>';
	}

	/**
	 * Save Custom Html.
	 *
	 * @param int $post_id Product ID.
	 *
	 * @return false|void
	 */
	public function save_custom_html( int $post_id ) {

		if ( ! ( isset( $_POST['woocommerce_meta_nonce'], $_POST['iwp_custom_html'], $_POST['is_coming_soon'], $_POST['iwp_contact_style'] ) || wp_verify_nonce( sanitize_key( $_POST['woocommerce_meta_nonce'] ), 'woocommerce_save_data' ) ) ) {
			return false;
		}

		$custom_html = wp_kses_post( wp_unslash( $_POST['iwp_custom_html'] ) );

		update_post_meta(
			$post_id,
			'iwp_custom_html',
			$custom_html
		);

		$coming_soon = filter_var( wp_unslash( $_POST['is_coming_soon'] ), FILTER_SANITIZE_STRING );

		update_post_meta(
			$post_id,
			'is_coming_soon',
			$coming_soon
		);

		$iwp_contact_style = wp_kses_post( wp_unslash( $_POST['iwp_contact_style'] ) );
		update_post_meta(
			$post_id,
			'iwp_contact_style',
			$iwp_contact_style
		);
	}

	/**
	 * Save fields price percentage.
	 *
	 * @param int $post_id Post ID.
	 *
	 * @return false|void
	 */
	public function save_fields_price_percentage( int $post_id ) {
		if ( ! ( isset( $_POST['woocommerce_meta_nonce'], $_POST['costs_product'], $_POST['tax_product'], $_POST['margin_product'] ) || wp_verify_nonce( sanitize_key( $_POST['woocommerce_meta_nonce'] ), 'woocommerce_save_data' ) ) ) {
			return false;
		}

		$costs_product  = filter_var( wp_unslash( $_POST['costs_product'] ), FILTER_SANITIZE_NUMBER_INT );
		$tax_product    = filter_var( wp_unslash( $_POST['tax_product'] ), FILTER_SANITIZE_NUMBER_INT );
		$margin_product = filter_var( wp_unslash( $_POST['margin_product'] ), FILTER_SANITIZE_NUMBER_INT );

		$max_value = $costs_product + $tax_product + $margin_product;

		if ( $max_value > 100 ) {
			update_post_meta( $post_id, 'error_percent', 1 );

			return false;
		}

		update_post_meta( $post_id, 'costs_product', $costs_product );
		update_post_meta( $post_id, 'tax_product', $tax_product );
		update_post_meta( $post_id, 'margin_product', $margin_product );
		update_post_meta( $post_id, 'error_percent', 0 );
	}

	/**
	 * Notification error.
	 *
	 * @return void
	 */
	public function notification() {
		printf(
			'<div id="pcs-woocommerce-nope" class="notice notice-error is-dismissible"><p>%s</p></div>',
			esc_html(
				__( 'You entered an incorrect percentage value, it cannot be more than 100%', 'north' ),
			)
		);
	}

	/**
	 * Output Custom Html.
	 */
	public function add_custom_html(): void {
		global $post;

		$custom_html = get_post_meta( $post->ID, 'iwp_custom_html', true );


		if ( empty( $custom_html ) ) {
			$custom_html = get_theme_mod( 'iwp_custom_html' );
		}

		if ( ! empty( $custom_html ) ) {
			echo '<div class="iwp-custom-html">';
			echo wp_kses_post( $custom_html );
			echo '</div>';
		}

	}

	/**
	 * Add Sizing Guide.
	 *
	 * @return void
	 */
	public function add_sizing_guide(): void {
		$thb_id               = get_the_ID();
		$sizing_guide         = get_post_meta( $thb_id, 'sizing_guide', true );
		$sizing_guide_content = get_post_meta( $thb_id, 'sizing_guide_content', true );

		if ( 'on' === $sizing_guide ) {
			?>
			<a href="#sizing-popup" rel="inline" class="sizing_guide" data-class="upsell-popup">
				<?php get_template_part( 'assets/img/svg/sizing_guide.svg' ); ?>
				<?php esc_html_e( 'Find your size', 'north' ); ?>
			</a>
			<aside id="sizing-popup" class="mfp-hide theme-popup text-left">
				<div class="theme-popup-content">
					<?php echo do_shortcode( $sizing_guide_content ); ?>
				</div>
			</aside>
			<?php
		}
	}

	/**
	 * Change Checkout Button.
	 *
	 * @return void
	 */
	public function change_checkout_button(): void {
		echo '<a href="' . esc_url( wc_get_checkout_url() ) . '" class="button checkout wc-forward">' . esc_html__( 'Quick Order', 'north' ) . '</a>';
	}

	/**
	 * Add Carbone Fields.
	 *
	 * @return void
	 */
	public function add_carbon_field(): void {
		Container::make(
			'term_meta',
			__( 'Custom Block in Category', 'north' )
		)->where( 'term_taxonomy', '=', 'product_cat' )->add_fields(
			[
				Field::make( 'rich_text', 'crb_custom_description', __( 'Custom Block', 'north' ) ),
			]
		);
	}

	/**
	 * Change Html Out of Stock.
	 *
	 * @param string $html    HTML.
	 * @param object $product WC Product.
	 *
	 * @return string
	 */
	public function custom_variable_html( $html, $product ) {

		if ( ! $product->is_in_stock() ) {
			ob_start();
			$shortcode = get_theme_mod( 'iwp_coming_soon' );
			?>
			<p class="coming-soon"><?php esc_html_e( 'Coming Soon', 'north' ); ?></p>
			<?php echo do_shortcode( $shortcode ); ?>
			<?php
			$html = ob_get_clean();
		}

		return $html;
	}

	/**
	 * Add Product category image.
	 *
	 * @return void
	 */
	public function add_image_size(): void {
		add_image_size( 'product-category-thumb', 2000, 350, [ 'top', 'center' ] );
	}

	/**
	 * Update product badge.
	 *
	 * @return void
	 */
	public function new_product_badge(): void {
		global $post, $product;
		if ( thb_out_of_stock() ) {
			echo '<span class="badge out-of-stock">' . esc_html__( 'Join in waiting list', 'north' ) . '</span>';
		} elseif ( $product->is_on_sale() ) {
			if ( ot_get_option( 'shop_sale_badge', 'text' ) == 'discount' ) {
				if ( $product->get_type() == 'variable' ) {
					$available_variations = $product->get_available_variations();
					$maximumper           = 0;
					for ( $i = 0; $i < count( $available_variations ); ++ $i ) {
						$variation_id      = $available_variations[ $i ]['variation_id'];
						$variable_product1 = new WC_Product_Variation( $variation_id );
						$regular_price     = $variable_product1->get_regular_price();
						$sales_price       = $variable_product1->get_sale_price();
						$percentage        = $sales_price ? round( ( ( $regular_price - $sales_price ) / $regular_price ) * 100 ) : 0;
						if ( $percentage > $maximumper ) {
							$maximumper = $percentage;
						}
					}
					echo apply_filters( 'woocommerce_sale_flash', '<span class="badge onsale perc">&darr; ' . $maximumper . '%</span>', $post, $product );
				} elseif ( $product->get_type() == 'simple' ) {
					$percentage = round( ( ( $product->get_regular_price() - $product->get_sale_price() ) / $product->get_regular_price() ) * 100 );
					echo apply_filters( 'woocommerce_sale_flash', '<span class="badge onsale perc">&darr; ' . $percentage . '%</span>', $post, $product );
				} elseif ( $product->get_type() == 'external' ) {
					$percentage = round( ( ( $product->get_regular_price() - $product->get_sale_price() ) / $product->get_regular_price() ) * 100 );
					echo apply_filters( 'woocommerce_sale_flash', '<span class="badge onsale perc">&darr; ' . $percentage . '%</span>', $post, $product );
				}
			} else {
				echo apply_filters( 'woocommerce_sale_flash', '<span class="badge onsale">' . esc_html__( 'Sale', 'north' ) . '</span>', $post, $product );
			}
		} else {
			$postdate      = get_the_time( 'Y-m-d' );          // Post date
			$postdatestamp = strtotime( $postdate );           // Timestamped post date
			$newness       = ot_get_option( 'shop_newness', 7 );
			if ( ( time() - ( 60 * 60 * 24 * $newness ) ) < $postdatestamp ) { // If the product was published within the newness time frame display the new badge
				echo '<span class="badge new">' . esc_html__( 'Just Arrived', 'north' ) . '</span>';
			}
		}
	}
}
