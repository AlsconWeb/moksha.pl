<?php
/**
 * Changing the order of displaying information in the product card.
 *
 * @package IWP
 */

namespace IWP;

/**
 * ChangeProductCard class file
 */
class ChangeProductCard {
	/**
	 * Pull action to remove.
	 *
	 * @var array[]
	 */
	private array $remove_actions_pull;

	/**
	 * Pull action to add.
	 *
	 * @var array[]
	 */
	private array $add_action_pull;

	/**
	 * ChangeProductCard construct.
	 */
	public function __construct() {
		$this->remove_actions_pull = [
			[
				'action'   => 'woocommerce_single_product_summary',
				'function' => 'woocommerce_template_single_price',
				'priority' => 10,
			],
		];

		$this->add_action_pull = [
			[
				'action'   => 'woocommerce_before_single_variation',
				'function' => 'woocommerce_template_single_price',
				'priority' => 30,
			],
			[
				'action'   => 'woocommerce_single_product_summary',
				'function' => [ $this, 'show_contact_style' ],
				'priority' => 35,
			],
		];
		$this->init();
	}

	/**
	 * Init function.
	 *
	 * @return void
	 */
	private function init(): void {
		$this->iwp_remove_action();
		$this->iwp_add_action();
	}

	/**
	 *  Remove Action.
	 *
	 * @return void
	 */
	public function iwp_remove_action(): void {
		foreach ( $this->remove_actions_pull as $action ) {
			remove_action( $action['action'], $action['function'], $action['priority'] );
		}
	}

	/**
	 * Add Action.
	 *
	 * @return void
	 */
	public function iwp_add_action(): void {
		foreach ( $this->add_action_pull as $action ) {
			add_action( $action['action'], $action['function'], $action['priority'] );
		}
	}

	/**
	 * Output Custom Html.
	 */
	public function show_contact_style(): void {
		global $post;

		$custom_html = get_post_meta( $post->ID, 'iwp_contact_style', true );

		if ( empty( $custom_html ) ) {
			$custom_html = get_theme_mod( 'iwp_contact_style' );
		}

		if ( ! empty( $custom_html ) ) {
			echo '<div class="iwp-custom-html">';
			echo wp_kses_post( $custom_html );
			echo '</div>';
		}

	}
}
